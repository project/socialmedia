INTRODUCTION
------------------

The Social media module helps to integrate your website with social media
sites such as Facebook, Twitter, etc.

REQUIREMENTS
-----------------------

This module, while doesn't require, recommends the Widgets module:

https://www.drupal.org/project/widgets

INSTALLATION
-------------------------

To install Drupal modules, follow the instructions listed at Drupal.org.

https://www.drupal.org/node/895232

CONFIGURATION
--------------------

1. Go to Admin > Module and make sure the Social Media module and Widgets
module are enabled.

2. Go to Admin > Configuration > Media settings and on the platforms, make
sure to check the social media platforms that you want to enable on your site
and uncheck the social media platforms that you do not want on your site.

3. Other settings include, Default Widget Appearance, default link attributes
and Icon Sets (Optional) which can be customized.

4. Save the configuration and go to Profile settings.

5. Setup the social media profile information such as profile URL and username.

6. Configure your Widget.

7. Display widget sets. You can display the Widget using blocks.
